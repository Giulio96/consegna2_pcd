package controller;

import view.FolderSelectionGUI;

public class FileAnalyzer {

	public static void main(String[] args) {
		SearchController searchController = new SearchController();
		FolderSelectionGUI fs = new FolderSelectionGUI(searchController);

		fs.show();
	}

}
