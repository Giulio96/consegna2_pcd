package controller;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class SearchController {

	List<File> files;
	
	public SearchController() {
		this.files = new ArrayList<>();
	}
	
	public List<File> startResearch(File path) {
		List<File> files = new ArrayList<>();
		for(File entry : path.listFiles()){
			if(entry.isDirectory()) {
				files.addAll(this.startResearch(entry));
			}else {
				files.add(entry);
			}
		}
		return files;
	}
}
