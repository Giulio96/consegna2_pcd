package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.TitledBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Utilities;

import controller.SearchController;

public class FolderSelectionGUI {
	
	private JFrame frame = new JFrame("Select Folder");
	private JPanel upperPanel = new JPanel();
	private JPanel centralPanel = new JPanel();
	private JPanel includedPanel = new JPanel();
	private JPanel exchangePanel = new JPanel();
	private JPanel excludedPanel = new JPanel();
	private JPanel controlPanel = new JPanel();
	private JPanel bottomPanel = new JPanel();
	private JPanel resultPanel = new JPanel();
	private JTextField field = new JTextField(30);
	private JTextArea area = new JTextArea(13,60);
	private JTextArea includedArea = new JTextArea(15,22);
	private JTextArea excludedArea = new JTextArea(15,22);

	private JLabel label = new JLabel("Seleziona una directory ");
	private JButton select = new JButton("Select");
	private JButton start = new JButton("Start");
	private JButton exclude = new JButton(">>");
	private JButton include = new JButton("<<");
	private JButton startResearch = new JButton("Start");
	private JButton stopResearch = new JButton("Stop");
	private JFileChooser fileChooser = new JFileChooser();
	private SearchController controller;
	private static final String defaultPath = "../Consegna2/Files";
	private String selectedLine;
	private List<String> excludedFiles = new ArrayList<>();
	private List<String> includedFiles = new ArrayList<>();
	DefaultHighlighter highlighter =  (DefaultHighlighter)this.includedArea.getHighlighter();
    DefaultHighlighter.DefaultHighlightPainter painter = new DefaultHighlighter.DefaultHighlightPainter( Color.RED );
    DefaultHighlighter highlighter2 =  (DefaultHighlighter)this.excludedArea.getHighlighter();
    DefaultHighlighter.DefaultHighlightPainter painter2 = new DefaultHighlighter.DefaultHighlightPainter( Color.RED );
    
	public FolderSelectionGUI(SearchController controller) {
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.setSize(700,700);
		this.frame.setBackground(Color.white);
		
		setUpperPanel();
		this.frame.getContentPane().add(this.upperPanel, BorderLayout.NORTH);
		
		setCentralPanel();
		this.frame.getContentPane().add(this.centralPanel, BorderLayout.CENTER);
		
		setBottomPanel();
		this.frame.getContentPane().add(this.bottomPanel, BorderLayout.SOUTH);
		
	    fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

	    this.controller = controller;
		this.registerHandlers();
	}
	
	
	private void setUpperPanel() {
		this.field.setEditable(false);
		this.includedArea.setEditable(false);
		this.excludedArea.setEditable(false);
		this.area.setEditable(false);

		this.upperPanel.setPreferredSize(new Dimension(100,60));
		this.upperPanel.add(label);
		this.upperPanel.add(field);
		this.upperPanel.add(select);
		this.upperPanel.add(start);
		this.field.setText(defaultPath);
	}
	
	private void setCentralPanel() {
		
		TitledBorder border1 = new TitledBorder(BorderFactory.createLineBorder(Color.blue), "Included");
	    border1.setTitleJustification(TitledBorder.CENTER);
	    border1.setTitlePosition(TitledBorder.TOP);
	    JScrollPane scroll = new JScrollPane(this.includedArea);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		this.includedPanel.setPreferredSize(new Dimension(250,280));
		this.includedPanel.setBorder(border1);
		this.includedPanel.add(scroll);
		
		this.centralPanel.add(this.includedPanel, BorderLayout.WEST);
		
		
		this.exchangePanel.setPreferredSize(new Dimension(100,80));
		this.exchangePanel.add(this.include, BorderLayout.CENTER);
		this.exchangePanel.add(this.exclude, BorderLayout.CENTER);
		this.centralPanel.add(this.exchangePanel, BorderLayout.CENTER);
		
		TitledBorder border2 = new TitledBorder(BorderFactory.createLineBorder(Color.blue), "Excluded");
	    border2.setTitleJustification(TitledBorder.CENTER);
	    border2.setTitlePosition(TitledBorder.TOP);
	    JScrollPane scroll2 = new JScrollPane(this.excludedArea);
        scroll2.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
	    this.excludedPanel.setBorder(border2);
		this.excludedPanel.setPreferredSize(new Dimension(250,280));
		this.excludedPanel.add(scroll2);
		this.centralPanel.add(this.excludedPanel, BorderLayout.EAST);
		
		this.controlPanel.setPreferredSize(new Dimension(600,50));
		this.controlPanel.add(this.startResearch, BorderLayout.CENTER);
		this.controlPanel.add(this.stopResearch, BorderLayout.CENTER);

		
		this.centralPanel.add(this.controlPanel, BorderLayout.SOUTH);
		
		
	}
	
	private void setBottomPanel() {		
		TitledBorder border2 = new TitledBorder(BorderFactory.createLineBorder(Color.blue), "RESULTS");
	    border2.setTitleJustification(TitledBorder.CENTER);
	    border2.setTitlePosition(TitledBorder.TOP);
	    JScrollPane scroll = new JScrollPane(this.area);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		this.resultPanel.setPreferredSize(new Dimension(630,250));
		this.resultPanel.setBorder(border2);
		this.resultPanel.add(scroll);

		this.bottomPanel.add(resultPanel);
	}
	
	
	
	private void registerHandlers() {
		this.select.addActionListener(e -> {
			this.selectFolder();
		});
		this.start.addActionListener(e -> {
			List<File> files = this.controller.startResearch(new File(this.field.getText()));
			for(File f : files) {
				this.includedArea.append(f.toString()+"\n");
				this.includedFiles.add(f.toString());
			}
		});
		
		this.include.addActionListener(e -> {
			this.excludedFiles.remove(this.selectedLine);
			this.includedFiles.add(this.selectedLine);
			this.printIncluded();
			this.printExcluded();
		});
		
		this.exclude.addActionListener(e -> {
			this.includedFiles.remove(this.selectedLine);
			System.out.println("REMOVEING:" +this.selectedLine+ "SIZE : "+this.includedFiles.size());
			this.excludedFiles.add(this.selectedLine);
			this.printIncluded();
			this.printExcluded();
		});
		
		this.includedArea.addMouseListener(new MouseAdapter() {
	         public void mouseClicked(MouseEvent e) {
	            if (e.getButton() != MouseEvent.BUTTON1) {
	               return;
	            }
	            int offset = includedArea.viewToModel(e.getPoint());

	            try {
	               int rowStart = Utilities.getRowStart(includedArea, offset);
	               int rowEnd = Utilities.getRowEnd(includedArea, offset);
	               String selectedLine = includedArea.getText().substring(rowStart, rowEnd);
	               FolderSelectionGUI.this.selectedLine = selectedLine+"\n";
	               FolderSelectionGUI.this.highlighter.removeAllHighlights();
	               FolderSelectionGUI.this.highlighter2.removeAllHighlights();
	               FolderSelectionGUI.this.highlighter.addHighlight(rowStart, rowEnd,  FolderSelectionGUI.this.painter);
	               FolderSelectionGUI.this.selectedLine = selectedLine;
	               FolderSelectionGUI.this.include.setEnabled(false);
	               FolderSelectionGUI.this.exclude.setEnabled(true);
	            } catch (BadLocationException e1) {
	               e1.printStackTrace();
	            }
	         }
		});
		
		this.excludedArea.addMouseListener(new MouseAdapter() {
	         public void mouseClicked(MouseEvent e) {
	            if (e.getButton() != MouseEvent.BUTTON1) {
	               return;
	            }
	            
	            int offset = includedArea.viewToModel(e.getPoint());
	            try {
	               int rowStart = Utilities.getRowStart(excludedArea, offset);
	               int rowEnd = Utilities.getRowEnd(excludedArea, offset);
	               String selectedLine = excludedArea.getText().substring(rowStart, rowEnd);
	               FolderSelectionGUI.this.highlighter.removeAllHighlights();
	               FolderSelectionGUI.this.highlighter2.removeAllHighlights();
	               FolderSelectionGUI.this.highlighter2.addHighlight(rowStart, rowEnd,  FolderSelectionGUI.this.painter);
	               FolderSelectionGUI.this.selectedLine = selectedLine;
	               FolderSelectionGUI.this.include.setEnabled(true);
	               FolderSelectionGUI.this.exclude.setEnabled(false);
	            } catch (BadLocationException e1) {
	               e1.printStackTrace();
	            }
	         }
		});
	}
	
	private void selectFolder() {
		if (fileChooser.showOpenDialog(this.upperPanel) == JFileChooser.APPROVE_OPTION) { 
			this.field.setText(fileChooser.getSelectedFile().toString());
		} else {
		      System.out.println("No Selection ");
		}
	}
	
	private void printExcluded() {
		this.excludedArea.setText("");
		for(String s : this.excludedFiles) {
			this.excludedArea.append(s+"\n");
		}
	}
	
	private void printIncluded() {
		this.includedArea.setText("");
		for(String s : this.includedFiles) {
			this.includedArea.append(s+"\n");
		}
	}
	
	public void show() {
		this.frame.setVisible(true);
	}
}
